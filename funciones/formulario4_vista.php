<form>
    <div>
        <label for="radio">Radio</label>
        <input type="number" name="radio" id="radio" value="<?= $radio ?>">
    </div>
    <div>
        <label for="area">Area</label>
        <input type="number" name="area" id="area" readonly="true" value="<?= $area ?>">
    </div>
    
    <div>
        <button name="boton">Calcular</button>
    </div>
</form>

<svg width="800" height="800">
    <?= circulo($radio*4) ?>
</svg>



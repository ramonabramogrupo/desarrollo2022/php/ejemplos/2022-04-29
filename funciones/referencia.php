<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $a=10;
        $b=$a; // asignacion de valor
        $a=40; // $b sigue valiendo 10
        var_dump($b);
        
        $c=10;
        $d=&$c; // asignacion por referencia
        $c=40; // $d tambien vale 40
        var_dump($d);
        
        function valor($a,$b,$suma){
            $suma=$a+$b;
        }
        
        $n1=10;
        $n2=23;
        $s=0;
        valor($n1,$n2,$s);
        var_dump($s); // $s valdra 0
        
        
                function valor($a,$b,$suma){
            $suma=$a+$b;
        }
        
        function referencia($a,$b,&$suma){
            $suma=$a+$b;
        }
        
        
        $n11=10;
        $n21=23;
        $s1=0;
        referencia($n11,$n21,$s1);
        var_dump($s1); // $s valdra 23
        
        ?>
    </body>
</html>

<?php
    function calcular($radio,&$area){
       $area= pi() * pow($radio, 2);
    }
    
    function render($radio,$area){
        include './formulario4_vista.php';
    }
    
    function circulo($radio){
     return '<circle cx="100" cy="100" r="'. $radio .'" stroke="black" stroke-width="4" fill="black">';
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // inicializando las variables
        $radio=0;
        $area=0;
        
        // controlando si he pulsado el boton
        if(isset($_GET["boton"])){
            $radio=$_GET["radio"];
            calcular($radio,$area);
        }
        
        // mostrar el formulario
        render($radio,$area);
        
        
        ?>
    </body>
</html>

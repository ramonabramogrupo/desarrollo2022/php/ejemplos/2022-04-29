<?php
        /**
         * funcion para generar un formulario desde una plantilla
         * @param string $titulo titulo del formulario
         * @param string $etiqueta etiqueta del control
         * @param string $nombre nombre del control
         * @return string Formulario con el control generado
         */
        function dibujar($titulo,$etiqueta,$nombre){
            // utilizamos formulario.inc como
            // una plantilla
            // utilizaremos file_get_contents
            $contenido= file_get_contents("formulario.inc");
            $contenido=str_replace("{{titulo}}", $titulo, $contenido);
            $contenido=str_replace("{{etiqueta}}", $etiqueta, $contenido);
            $contenido=str_replace("{{nombre}}", $nombre, $contenido);
            return $contenido;
           
        }
        
                /**
         * funcion para generar un formulario desde una plantilla
         * @param string $titulo titulo del formulario
         * @param string $etiqueta etiqueta del control
         * @param string $nombre nombre del control
         * @return string Formulario con el control generado
         */
        
        function dibujarArray($titulo,$etiqueta,$nombre){
            // utilizamos formulario.inc como
            // una plantilla
            // utilizaremos file_get_contents
            $valores=[$titulo,$etiqueta,$nombre];
            $textos=["{{titulo}}","{{etiqueta}}","{{nombre}}"];
            $contenido= file_get_contents("formulario.inc");
            
            return str_replace($textos, $valores, $contenido);
           
        }
        
        function dibujar1($titulo,$etiqueta,$nombre){
            include "formulario_1.inc";
        }

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            echo dibujar(
                    "Introduccion de datos",
                    "Numero 1",
                    "numero1"
                    );
            
            echo dibujarArray("Introduccion de numero",
                    "Numero",
                    "numero"
                    );
            
            echo dibujar1(
                    "Introduccion de numero",
                    "Numero",
                    "numero"
                    );
               
        
            
            
        ?>
    </body>
</html>

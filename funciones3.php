<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $a="Ejemplo de clase";
        
        // sustitir la o por un -
        $final=str_replace(
                "o", // caracter a sustituir
                "-", // caracter de reemplazo
                $a // el texto donde sustituye los caracteres
                );
        echo $final; // La salida es "Ejempl- de clase"
        
        // sustituir todas las e por -
        $final= str_replace("e","-",$a);
        echo $final; // La salida es "Ej-mplo d- clas-"
        
        // sustituir todas las e por -
        $final= str_ireplace("e","-",$a);
        echo $final; // La salida es "-j-mplo d- clas-"
        
        // sustituir clase por aula
        
        $final= str_replace("clase", "aula", $a);
        echo $final; // La salida es "Ejemplo de aula"
        ?>
    </body>
</html>
